// actions
export default {
    recalculateTotal(context) {
        let total = 0;
        context.state.products.forEach((product) => {
            total += (product.num * product.price);
        });

        context.commit('setTotal', { total });
    },
}; // actions
