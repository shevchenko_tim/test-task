// mutations
export default {
    changeProductNum(state, { num, product }) {
        const index = state.products.findIndex((p) => p.name === product.name);
        state.products[index].num = num !== '' ? num : 0;
    },
    setTotal(state, { total }) {
        state.total = total;
    },
}; // mutations
